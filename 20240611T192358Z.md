In der Regel ist es in formellen Kontexten, wie dem Anbieten eines Artikels zur Veröffentlichung, angemessen, die Redaktion zu siezen. Das gilt unabhängig von der politischen Ausrichtung der Zeitung. Eine höfliche und respektvolle Ansprache zeigt Professionalität und wird in den meisten Fällen positiv aufgenommen.

Ein Beispiel für eine formelle Anrede könnte so aussehen:

"Sehr geehrte Damen und Herren der Redaktion,

ich möchte Ihnen einen Artikel zur Veröffentlichung anbieten..."

Falls Sie jedoch bereits eine persönliche Beziehung zu jemandem in der Redaktion haben oder wissen, dass dort ein informellerer Ton üblich ist, könnten Sie auch das "ihr" verwenden. In solchen Fällen wäre es ratsam, sich an den bisherigen Kommunikationsstil anzupassen.

Ein Beispiel für eine informellere Anrede könnte so aussehen:

"Liebe Redaktion,

ich habe einen Artikel geschrieben, den ich euch gerne zur Veröffentlichung anbieten würde..."

Im Zweifelsfall ist es jedoch sicherer und professioneller, die formelle Anrede zu wählen.

# Tue 11 Jun 19:23:58 CEST 2024 - Wenn ich einer linkspolitisch gerichteten Zeitung einen Artikel für Veröffentlichung anbiete, sollte ich die Redaktion „Sie“ oder „ihr“ nennen?